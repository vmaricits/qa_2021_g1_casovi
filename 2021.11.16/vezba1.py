albumi = {
    1981:{
        "naziv"   : "Thriller",
        "grupa"   : "Michael Jackson",
        "pesme"   : "Thriller,Beat it,Billy Jean",
        "godina"  : 1981
    },
    2001:{
        "naziv"  : "Brave new world",
        "grupa"  : "Iron Maiden",
        "pesme"  : "Brave new world",
        "godina" : 2001
    }
}

while True:
    godina = input("Unesi godinu: ")
    da_li_je_broj   = godina.isnumeric()
    if not da_li_je_broj:
        print("Nisi uneo broj")
        continue
    godina          = int(godina)
    postoji         = godina in albumi
    if not postoji:
        print("Album za trazenu godinu ne postoji")
        continue
    album           = albumi[godina]
    print(album)
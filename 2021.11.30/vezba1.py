import random

print("Nas program")

zamisljeni_broj     = random.randint(1,5) 

uneta_vrednost      = input("Unesi broj: ") 

if not uneta_vrednost.isnumeric():
    print("Nisi uneo broj")
    exit(0)

broj                = int(uneta_vrednost) 
pogodak             = zamisljeni_broj == broj

print("Zamisljeni broj:",zamisljeni_broj,"Uneti broj:",broj)
print("Pogodak:",pogodak)

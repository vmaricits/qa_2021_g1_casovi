import random 

#do 40 - sve ok
#40-50 upozorenje
#50-60 kazna
#>60 dozvola

dozvoljena_brzina   = 40
kazna               = 50
oduzimanje_dozvole  = 60

trenutna_brzina     = random.randint(30,100)

if trenutna_brzina <= dozvoljena_brzina:
    print(trenutna_brzina,"\U0001F600")
elif trenutna_brzina <= kazna:
    print(trenutna_brzina,"\U0001F612")
elif trenutna_brzina <= oduzimanje_dozvole:
    print(trenutna_brzina,"\U0001F620")
else:
    print(trenutna_brzina,"\U0001F62D")
    

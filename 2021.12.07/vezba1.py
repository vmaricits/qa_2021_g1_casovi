import time
import random
 
print("Start game...") 
vreme           = 0
max_trajanje    = 3 * 60
default_health  = 100
health          = default_health

while vreme < max_trajanje:
    print(f"Igra radi {vreme}")

    health -= random.randint(0,20)

    print(health)

    if health <= 0:
        print("mrtav je igrac")
        health = default_health 
        
    vreme += 1
    time.sleep(1)

print("End game")
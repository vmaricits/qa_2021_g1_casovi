import random,time

class Tenk:
    naziv   = ""
    brzina  = 0
    health  = 0
    damage  = 0

tiger           = Tenk()
tiger.naziv     = "Tiger"
tiger.brzina    = random.randint(1,5)
tiger.damage    = random.randint(10,50)
tiger.health    = random.randint(80,150)

panzer          = Tenk()
panzer.naziv    = "Panzer"
panzer.brzina   = random.randint(1,5)
panzer.damage   = random.randint(10,50)
panzer.health   = random.randint(80,150)

print(tiger.naziv,tiger.health,tiger.damage)
print(panzer.naziv,panzer.health,panzer.damage)

while panzer.health > 0 and tiger.health > 0:
    panzer.health   -= tiger.damage
    tiger.health    -= panzer.damage
    print(tiger.naziv,tiger.health,tiger.damage)
    print(panzer.naziv,panzer.health,panzer.damage)
    time.sleep(1)

print("Pobednik je",(panzer.naziv if panzer.health > tiger.health else tiger.naziv) )




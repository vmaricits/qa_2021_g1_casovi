konzole = [
    ["Nintendo Switch Mario Edition",48000,[["Super Mario",3000],["Zelda",3200]]],
    ["Sony 5 Soccer",80000,[["Pro Evolution Soccer",4000],["Crash Bandicot",2200],["Mortal Kombat 11",5000]]],
    ["X-Box",130000,[["Red Dead Redemption 2",5000],["Age of Empires",3100],["Spiderman",4800],["Division",5000]]]
]


for konzola in konzole:
    print("Naziv:",konzola[0],"Cena:",konzola[1])
    print("Igre:")
    for igra in konzola[2]:
        print("    ",igra[0],"(",igra[1],")")
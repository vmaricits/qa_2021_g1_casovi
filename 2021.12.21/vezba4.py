import requests 
igre = requests.get("http://jadnik.com/igre.json").json() 

broj_gresaka = 0
for indeks_konzole,konzola in enumerate(igre):
    print("Testira se:",konzola[0])
    igre_konzole = konzola[2]
    for indeks_igre,igra in enumerate(igre_konzole):
        cena = requests.get(f"http://jadnik.com/igre.php?konzola={indeks_konzole}&igra={indeks_igre}").text
        print("Cena za igru:",igra[0],"Ocekivana cena:",igra[1],"Dobijena cena:",cena)
        broj_gresaka += 1 if int(cena) != int(igra[1]) else 0 

print("#"*100)
print("Ukupno gresaka",broj_gresaka)
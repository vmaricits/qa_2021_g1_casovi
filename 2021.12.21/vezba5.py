import requests

proizvodi   = open("proizvodi.txt","r").read().splitlines() 
popusti     = open("popusti.txt","r").read().splitlines()
prodavnice  = open("prodavnice.txt","r").read().splitlines()

broj_gresaka = 0

for prodavnica in prodavnice:
    print("#"*10,"Testiranje prodavnice:",prodavnica,"#"*10)

    for p in proizvodi:
        p = p.split(",")
        print("Proizvod:",p[0],"Osnovna cena:",p[1])
        for po in popusti:
            po = po.split(" ")
            naziv_popusta       = po[0]
            vrednost_popusta    = float(po[1])
            cena_sa_popustom    = vrednost_popusta * float(p[1])
            print("Provera popusta:",naziv_popusta)
            odgovor = requests.get(f"http://{prodavnica}/porez.php?cena={p[1]}&popust={naziv_popusta}").text
            odgovor = float(odgovor)
            if odgovor != cena_sa_popustom:
                broj_gresaka += 1 

print("Ukupno gresaka:",broj_gresaka)
def sabiranje(a,b):
    """
    Sabiranje brojeva

    Args:
        a: prvi broj
        b: drugi broj
    """
    print(a+b)

def oduzimanje(a,b):
    """
    Komanda oduzima brojeve tako sto se ubace dva broja, i drugi se oduzme od prvog

    Argumenti: 
        a: prvi broj 
        b: drugi broj

    """
    print(a-b)
 
def dovla():
    """
    Ova komanda ispisuje istinu
    """
    print("Dovla the greatest teacher!!!")
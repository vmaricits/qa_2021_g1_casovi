x = 0
y = 0

def nacrtaj(w,h):
    """
    Iscrtava se ekran i u okviru ekrana mario, na poziciji x i y

    Args:
        w: sirina ekrana
        h: visina ekrana

    """
    for i in range(h):
        for j in range(w):
            if i == y and j == x:
                print("#",end="")
            else:
                print(" ",end="")
        print()    

def levo():
    """
    Mario se pomera za jedan korak u levo
    """
    global x
    x -= 1

def desno():
    """
    Mario se pomera za jedan korak u desno
    """
    global x
    x += 1

def prikazi():
    """
    Prikazuje se trenutna pozicija Marija
    """
    print(x,y)
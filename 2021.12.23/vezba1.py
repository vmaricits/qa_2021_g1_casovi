from selenium.webdriver import Chrome
import threading

def jedan_test(i):
    browser = Chrome() 
    browser.get("http://jadnik.com") 
    browser.find_element_by_id("inputEmail").send_keys(f"user_{i}")
    browser.find_element_by_id("inputPassword").send_keys(f"password_{i}") 
    browser.find_element_by_tag_name("button").click() 
    browser.save_screenshot(f"slike/user_{i}.png") 
    browser.quit()

for i in range(1,31):
    threading.Thread(None,jedan_test,args=(i,)).start()


def prikaz(ime,ime1,ime2):
    """
    Prikazuje informacije o razlicitim personama
    Args:
        ime: prva persona
        ime1: druga persona
        ime3: treca persona
    """
    print(f"{ime} vise ne drzi cas")
    print(f"{ime1} razmislja o igrama za Switch")
    print(f"{ime2} razmislja o ko zna cemu") 
 
# prikaz("Andrija","Goran","Dovla")
# prikaz("Elsa","Ana","Bojana")
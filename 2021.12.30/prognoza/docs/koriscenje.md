## Koriscenje

Da biste koristili aplikaciju treba Vam Python.
Aplikacija se startuje kucanjem komande:

<b>python app.py</b>

Nakon starta, aplikacija zahteva od korisnika unos grada, nakon cega prikazuje aktuelnu prognozu za taj grad.


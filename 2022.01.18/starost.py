def starost(age):
    if age < 25:
        return 2
    elif age < 65:
        return 55
    else:
        return 80

import random

#Equivalent partitioning
# age1 = random.randint(0,24)
# age2 = random.randint(25,64)
# age3 = random.randint(65,100) 
# print(age1,age2,age3) 
# print(starost(age1),starost(age2),starost(age3)) 

#Border Values Analysis
print("Za 24:",starost(24))
print("Za 25:",starost(25))
print("Za 64:",starost(64))
print("Za 65:",starost(65))
print("Za 100:",starost(100))
print("Za 0:",starost(0))

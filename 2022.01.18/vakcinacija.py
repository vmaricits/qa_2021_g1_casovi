import starost,zdravlje

loc = {
    "en":{"_unesi_godiste":"Enter your age","_unesi_broj_hronicnih_oboljenja":"Enter your chronical diseases count"},
    "sr":{"_unesi_godiste":"Unesi broj godina","_unesi_broj_hronicnih_oboljenja":"Unesi broj hronicnih oboljenja"}
} 

jezik = input("Language: ")

godine      = int(input(f"{loc[jezik]['_unesi_godiste']}: "))
zdr         = int(input(f"{loc[jezik]['_unesi_broj_hronicnih_oboljenja']}: "))
rezultat    = starost.starost(godine)
hronicno    = zdravlje.zdravlje(zdr)
if hronicno or rezultat > 50:
    print("Moras da se vakcinises")
else:
    print("Ne moras da se vakcinises")
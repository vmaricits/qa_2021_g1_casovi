import requests

provere = {
    "123456.78":"stotina dvadesettri hiljade četiri stotine pedesetšest dinara i sedamdesetosam para",
    "100253.00":"stotina hiljada dve stotine pedesettri dinara",
    "456789.11":"četiri stotine pedesetšest hiljada sedam stotina osamdesetdevet dinara i jedanaest para"
}

for kljuc,vrednost in provere.items():   
    odgovor = requests.get(f"http://gresnik.com?amount={kljuc}").text
    print(odgovor == vrednost)
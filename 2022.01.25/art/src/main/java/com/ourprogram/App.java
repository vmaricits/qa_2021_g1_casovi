package com.ourprogram;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Slika s = new Slika();
        s.autor = "Bojana Pavlovic";
        s.naziv = "Rapsodija u plavom";
        s.opis  = "Ovde je autorka predstavila plavetnilo svog poznavanja programskog jezika Java";
        s.prikazi();

        Slika s1 = new Slika();
        s1.autor = "Vladimir Maric";
        s1.naziv = "Vayne, the dark shadow";
        s1.opis  = "Ovo je slika Vayne, mog omiljenog ADC heroja";
        s1.prikazi();

    }
}

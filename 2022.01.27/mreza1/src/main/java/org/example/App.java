package org.example;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class App
{
    public static void main( String[] args ) throws IOException {

        ServerSocket server = new ServerSocket(1234);
        while(true){
            Socket klijent = server.accept();
            InputStream usi = klijent.getInputStream();
            OutputStream usta = klijent.getOutputStream();
            InputStreamReader bis = new InputStreamReader(usi);
            BufferedReader br = new BufferedReader(bis);

            String linija;
            while(!(linija=br.readLine()).isEmpty()){
                System.out.println(linija);
            }

//        int bajt;
//        for(int i=0;i<75;i++){
//            bajt = bis.read();
//            System.out.print((char)bajt);
//        }
            usta.write("HTTP/1.1 200 Ok\r\nContent-length: 11\r\n\r\nHello World".getBytes());
        }

    }
}

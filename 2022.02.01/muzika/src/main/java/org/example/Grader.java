package org.example;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Grader {
    /**
     * Metoda za unos muzicara
     * @return String
     */
    String unos_muzicara(){
        Scanner skener = new Scanner(System.in);
        String odabrani_muzicar = skener.nextLine();
        return odabrani_muzicar;
    }

    int vrati_ocenu(String naziv){
        Map muzicari = new HashMap();
        muzicari.put("linkin park",6);
        muzicari.put("2pac",4);
        muzicari.put("leonard cohen",7);
        muzicari.put("sakira",5);
        muzicari.put("eric clapton",8);
        int ocena = (int)muzicari.get(naziv);
        return ocena;
    }

    void oceni(){
        String muzicar  = unos_muzicara();
        int ocena       = vrati_ocenu(muzicar);
        System.out.println(ocena);
    }

}

package org.example;

import java.util.Map;

public class App
{
    public static void main( String[] args )
    {
        Karta k         = new Karta(1,1500,"Ko to tamo peva");
        Karta k1        = new Karta(2,1600,"Dizni na ledu");
        Karta k2        = new Karta(3,1700,"Pomorandzina kora");
        Karta k3        = new Karta(4,1500,"Ko to tamo peva");
        Karta k4        = new Karta(5,1200,"Dizni na ledu");

        ValidatorKarata vk = new ValidatorKarata();
        vk.validate(k);
        vk.validate(k1);
        vk.validate(k2);
        vk.validate(k3);
        vk.validate(k4);

        vk.revenue();

    }
}

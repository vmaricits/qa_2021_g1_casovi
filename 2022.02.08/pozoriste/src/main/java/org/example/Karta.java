package org.example;

public class Karta {
    int id;
    int cena;
    String predstava;

    Karta(int id, int cena, String predstava){
        this.id             = id;
        this.cena           = cena;
        this.predstava      = predstava;
    }

    void show(){
        System.out.println(this.id + " " + this.cena + " " + this.predstava);
    }

}

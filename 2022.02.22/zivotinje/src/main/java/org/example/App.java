package org.example;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Zivotinja z = new Zivotinja();
        Zivotinja.oglasavanje_psa = "vau";
        z.ime       = "Vibli";
        z.vrsta     = "Buldog";
        z.info();
        Zivotinja.domace_zivotinje();
        z.zvuk();
        System.out.println(Zivotinja.oglasavanje_psa);
    }
}

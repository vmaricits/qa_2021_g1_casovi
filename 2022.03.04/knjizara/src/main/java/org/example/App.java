package org.example;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        Biblioteka b = new Biblioteka();
        while(true){
            System.out.println("1. Nova knjiga, 2. Sve knjige, 3. Brisanje knjige, 4. Izlaz");
            int komanda = Integer.parseInt(scanner.nextLine());
            switch (komanda){
                case 1:
                    System.out.println("Unesi naziv:");
                    String naziv = scanner.nextLine();
                    System.out.println("Unesi isbn:");
                    String isbn = scanner.nextLine();
                    System.out.println("Unesi cenu:");
                    double cena = Double.parseDouble(scanner.nextLine());
                    Knjiga k = new Knjiga(naziv,isbn,cena);
                    b.dodaj(k);
                    break;
                case 2:
                    for(Knjiga k1 : b.sve_knjige()){
                        System.out.println(k1.naziv + " " + k1.imdb + " " + k1.cena);
                    }
                    break;
                case 3:
                    System.out.println("Unesi isbn");
                    b.brisi(scanner.nextLine());
                    break;
                case 4:
                    System.out.println("Aj cao");
                    System.exit(0);
                    break;
            }
        }

    }
}

package org.example;

import java.util.ArrayList;
import java.util.List;

public class Biblioteka {
    List<Knjiga> knjige = new ArrayList<>();
    void dodaj(Knjiga knjiga){
        knjige.add(knjiga);
    }
    List<Knjiga> sve_knjige(){
        return knjige;
    }
    void brisi(String isbn){
        for(Knjiga k : knjige){
            if(isbn.equals(k.imdb)){
                knjige.remove(k);
                break;
            }
        }
    }
}

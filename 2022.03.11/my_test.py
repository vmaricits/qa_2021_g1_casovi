import lib

def test_bet_100():
    ocekivano = 150.0
    dobijeno  = lib.bet(1.5,100)
    assert ocekivano == dobijeno

def test_bet_200():
    ocekivano   = 220.0
    dobijeno    = lib.bet(1.1,200)
    assert ocekivano == dobijeno
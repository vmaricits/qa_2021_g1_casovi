import lib,model

model.gljive = [{"naziv":"sampinjon","cena":150,"porez":0.2},{"naziv":"bukovaca","cena":170,"porez":0.2},{"naziv":"vrganj","cena":190,"porez":0.2}]

def test_get_gljive():
    ocekivano = ["sampinjon","bukovaca","vrganj"] 
    dobijeno  = lib.get_gljive() 
    if ocekivano == dobijeno:
        print("Ispravno")
    else:
        print("Neispravno")

def test_get_gljive_dict():
    ocekivano = [{"naziv":"sampinjon","cena":150,"porez":0.2},{"naziv":"bukovaca","cena":170,"porez":0.2},{"naziv":"vrganj","cena":190,"porez":0.2}]
    dobijeno = lib.get_gljive_dict()
    if ocekivano == dobijeno:
        print("Ispravno")
    else:
        print("Neispravno")

# test_get_gljive()
# test_get_gljive_dict()
import unittest
import lib
import model

class MojaTestKlasa(unittest.TestCase): 
    def setUp(self) -> None:
        model.gljive = [{"naziv":"sampinjon123","cena":150,"porez":0.2},{"naziv":"bukovaca","cena":170,"porez":0.2},{"naziv":"vrganj","cena":190,"porez":0.2}]

    def tearDown(self) -> None:
        print("Test je zavrsen")

    def test_get_gljive(self): 
        ocekivano = ["sampinjon123","bukovaca","vrganj"] 
        dobijeno  = lib.get_gljive() 
        self.assertEqual(ocekivano,dobijeno) 
    def test_get_gljive_dict(self): 
        ocekivano = [{"naziv":"sampinjon123","cena":150,"porez":0.2},{"naziv":"bukovaca","cena":170,"porez":0.2},{"naziv":"vrganj","cena":190,"porez":0.2}]
        dobijeno = lib.get_gljive_dict()
        self.assertEqual(ocekivano,dobijeno)
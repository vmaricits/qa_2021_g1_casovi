import requests

def test_filmovi():
    fajl = open("filmovi.txt","r") 
    linija = fajl.readline()
    while linija: 
        id_filma = linija.split(',')[0]
        odgovor = requests.get(f"http://gubitnik.com/film.php?id={id_filma}").text
        odgovor = odgovor.replace("|",",")
        assert odgovor.strip() == linija.strip()
        linija = fajl.readline() 

    fajl.close()



import requests

def req(ulaz_a,ulaz_b): 
    odgovor = requests.get(f"http://gubitnik.com/div.php?a={ulaz_a}&b={ulaz_b}").text
    odgovor = odgovor.split(" ")
    assert ulaz_a == int(odgovor[1])
    assert ulaz_b == int(odgovor[3])
    ocekivani_odgovor = round(ulaz_a / ulaz_b,4)
    print(ulaz_a,ulaz_b,ocekivani_odgovor)
    assert ocekivani_odgovor == round(float(odgovor[5]),4)

def test_full_range():
    for i in range(1,10,13):
        for j in range(1,10,13):
            req(i,j)

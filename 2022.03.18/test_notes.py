import notes

def test_all_notes():
    for ltr,note in notes.notes.items():
        assert ltr == notes.letter(note)

def test_all_letters():
    for ltr,note in notes.notes.items():
        assert note == notes.note(ltr)

def test_letter_existence():
    ocekivana_lista_slova = ["A","H","C","D","E","F","G"]
    for slovo in ocekivana_lista_slova:
        assert slovo in notes.notes

def test_letter_la():
    ulazna_vrednost     = "La"
    ocekivana_vrednost  = "A"
    dobijena_vrednost   = notes.letter(ulazna_vrednost)
    assert ocekivana_vrednost == dobijena_vrednost

def test_note_a():
    ulazna_vrednost     = "A"
    ocekivana_vrednost  = "La"
    dobijena_vrednost   = notes.note(ulazna_vrednost)
    assert ocekivana_vrednost == dobijena_vrednost

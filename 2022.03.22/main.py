#http://www.gubitnik.com/rating.php

import requests

def test_rating():
    res = requests.get("http://www.gubitnik.com/rating.php").text 
    dobijeno = dict([(i.split(",")[0],int(i.split(",")[1])) for i in res.replace("Rating,Total<hr>","").rstrip("<br>").split("<br>")])  
    ocekivano = {
        "R":0,
        "PG-13":0,
        "G":0,
        "NC-17":0,
        "PG":0
    }

    fajl = open("filmovi.txt","r")
    red = fajl.readline()
    while red:
        rating = red.split(",")[10].strip()
        ocekivano[rating] += 1
        red = fajl.readline()

    for i,j in ocekivano.items():
        assert i in dobijeno and dobijeno[i] == j  



package org.example;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

public class AppTest
{
    @Test
    public void testNaslov(){
        String ocekivano = "Opis naocara Ray Ban";
        String dobijeno = Naocari.opis1("Ray Ban");
        Assert.assertEquals(ocekivano,dobijeno);
    }
}

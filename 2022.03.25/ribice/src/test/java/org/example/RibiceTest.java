package org.example;

import org.junit.*;

public class RibiceTest {
    static Ribice r;

    @BeforeClass
    public static void priprema(){
        System.out.println("Dovla car");
        r = new Ribice();
    }

    @AfterClass
    public static void kraj(){
        r = null;
    }

    @Test
    public void latinToName(){
        String ocekivano = "Zuta ajkula";
        String dobijeno = r.ltn("Negaprion brevirostris");
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void nameToLatin(){
        String ocekivano = "Negaprion brevirostris";
        String dobijeno  = r.ntl("Zuta ajkula");
        Assert.assertEquals(ocekivano,dobijeno);
    }

}

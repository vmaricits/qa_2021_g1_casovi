package org.example;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class Cene {
    Map<String,Double> proizvodi = new HashMap<String,Double>(){
        {
            put("trotinet",100.0);
            put("bicikl",120.0);
            put("auto",300.0);
            put("avion",400.0);
            put("tenk",500.0);
        }
    };

    double porez(double cena){
        double p = 1.15;
        DecimalFormat df = new DecimalFormat("###.#");
        String broj = df.format(cena * p);
        return Double.parseDouble(broj);
    }
    boolean dostupno(String naziv){
        return proizvodi.containsKey(naziv);
    }
    double cena(String naziv){
        return proizvodi.get(naziv);
    }
}

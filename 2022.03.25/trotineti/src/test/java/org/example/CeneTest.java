package org.example;

import junit.framework.TestCase;
import org.junit.Test;

public class CeneTest extends TestCase {
    Cene c = new Cene();
    public void testDostupnost(){
        boolean dobijeno = c.dostupno("trotinet");
        boolean ocekivano = true;
        assertTrue(dobijeno);
        assertEquals(ocekivano,dobijeno);
    }
    public void testPoreza(){
        double dobijeno = c.porez(100.0);
        double ocekivano = 115.0;
        assertEquals(dobijeno,ocekivano);
    }
    public void testCena(){
        double dobijeno = c.cena("trotinet");
        double ocekivano = 100.0;
        assertEquals(ocekivano,dobijeno);
    }
}

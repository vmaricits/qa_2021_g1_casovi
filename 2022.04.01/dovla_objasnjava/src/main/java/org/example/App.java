package org.example;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Client cl = ClientBuilder.newClient();
        WebTarget wt = cl.target("http://gubitnik.com/products.php");
        String dobijeno = wt.request().get().readEntity(String.class);
        String ocekivano = "<a href='products.php'>Nazad</a>\nAvailable products<hr><a href='?id=1'>Fender Yngwie Malmsteen MN VW</a><br><a href='?id=2'>Hughes&Kettner GrandMeister Deluxe 40</a><br><a href='?id=3'>Kemper Profiler Stage</a><br>";
        System.out.println(ocekivano.equals(dobijeno));
    }
}

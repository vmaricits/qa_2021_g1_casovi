package com.packages;

public class App
{
    public static void main( String[] args )
    {
        Model m = new Model();
        int komada = m.getPackages().size();
        Package preuzeti_paket = m.getPackage("700mb");
        m.deletePackage(preuzeti_paket);
        komada = m.getPackages().size();
        System.out.println(komada);
    }
}

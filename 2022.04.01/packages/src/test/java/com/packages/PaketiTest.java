package com.packages;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class PaketiTest {
    @Test
    public void testGetPackages(){
        Model m = new Model();
        Map<String,Package> paketi = m.getPackages();
        Assert.assertNotNull(paketi);
    }
    @Test
    public void testAddPackage(){
        Package novi_paket = new Package("probnipaket",99999,999.99);
        Model m = new Model();
        m.addPackage(novi_paket);
        Package dodati_paket = m.getPackage("probnipaket");
        Assert.assertEquals(novi_paket,dodati_paket);
    }
    @Test
    public void testUpdatePackage(){
        Model m = new Model();
        Package paket = m.getPackage("700mb");
        paket.price = 5000;
        m.updatePackage(paket);
        Package uzeti_paket = m.getPackage("700mb");
        Assert.assertEquals(uzeti_paket.price,5000,0);
    }
    @Test
    public void testDelete(){
        Model m = new Model();
        int komada = m.getPackages().size();
        Assert.assertEquals(komada,4);
        Package preuzeti_paket = m.getPackage("700mb");
        m.deletePackage(preuzeti_paket);
        komada = m.getPackages().size();
        Assert.assertEquals(komada,3);
    }
}

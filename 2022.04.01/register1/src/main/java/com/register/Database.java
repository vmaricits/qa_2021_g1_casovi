package com.register;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    public static Connection conn;
    public static Connection connect(){
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/vezbe6","root","");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return conn;
    }
}

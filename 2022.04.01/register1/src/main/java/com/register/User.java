package com.register;

import java.sql.*;

public class User {
    public int id;
    public String username;
    public User(){ }
    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }

    /**
     * Metoda za registrovanje
     * @param username korisnicko ime
     * @param password sifra korisnika
     * @return User ulogovani korisnik
     */
    public static User register(String username, String password) {
        Connection conn = Database.connect();
        try {
            ResultSet rs = conn.createStatement().executeQuery("select * from users where username = '"+username+"' limit 1");
            if(!rs.next()){
                PreparedStatement st = conn.prepareStatement("insert into users values (null,'"+username+"','"+password+"')",Statement.RETURN_GENERATED_KEYS);
                st.execute();
                ResultSet ids = st.getGeneratedKeys();
                ids.next();
                return new User(ids.getInt(1),username);
            } else {
                return null;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}

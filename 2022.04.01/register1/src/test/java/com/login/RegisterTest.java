package com.login;

import com.register.Database;
import com.register.User;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.sql.Connection;
import java.sql.SQLException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegisterTest {

    @BeforeClass
    public static void ocisti(){
        Connection conn = Database.connect();
        try {
            conn.createStatement().execute("truncate table users");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void aaatestRegister(){
        User registrovani_korisnik = User.register("elza","123");
        Assert.assertNotNull(registrovani_korisnik);
        Assert.assertEquals("elza",registrovani_korisnik.username);
    }
    @Test
    public void bbbtestRegister1000(){
        for(int i=0;i<1000;i++){
            User registrovani_korisnik = User.register("user_"+i,"123");
            try {
                Database.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Assert.assertNotNull(registrovani_korisnik);
            Assert.assertEquals("user_"+i,registrovani_korisnik.username);
        }
    }
    @Test
    public void ccctestRegister1000Existin(){
        for(int i=0;i<1000;i++){
            User registrovani_korisnik = User.register("user_"+i,"123");
            try {
                Database.conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Assert.assertNull(registrovani_korisnik);
        }
    }
}

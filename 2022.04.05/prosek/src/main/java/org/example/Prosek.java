package org.example;

import java.util.List;

public class Prosek {

    public double izracunaj(List<Double> a) throws Exception{
        if(a==null || a.size()<1){
            throw new Exception("Ne valja lista");
        }
        double broj_ocena = a.size();
        double total = 0;
        for(double ocena : a){
            total += ocena;
        }
        return total / broj_ocena;
    }

}

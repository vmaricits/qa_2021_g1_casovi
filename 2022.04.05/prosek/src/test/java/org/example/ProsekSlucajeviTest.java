package org.example;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class ProsekSlucajeviTest {

    List<Double> ocene = new ArrayList<>();

    @Given("instanciranje objekta")
    public void probaInstanciranja(){
        System.out.println("Proba instanciranja");
        Prosek p = new Prosek();
        Assert.assertNotNull(p);
    }

    @When("sve petice")
    public void probaSvePetice(){
        ocene.add(5.0);
        ocene.add(5.0);
    }
    @Then("prosek pet")
    public void prosekPet() throws Exception {
        Prosek p = new Prosek();
        Assert.assertEquals(p.izracunaj(ocene),5.0,0);
    }
}

//package org.example;
//
//import org.junit.Assert;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ProsekTest
//{
//    @Test(expected = Exception.class)
//    public void izracunajPrazno() throws Exception {
//        Prosek p = new Prosek();
//        List<Double> brojevi = new ArrayList<>();
//        p.izracunaj(brojevi);
//    }
//
//    @Test
//    public void izracunajTest() throws Exception {
//        Prosek p = new Prosek();
//        List<Double> brojevi = new ArrayList<>();
//        brojevi.add(5.0);
//        brojevi.add(5.0);
//        double dobijeno = p.izracunaj(brojevi);
//        double ocekivano = 5.0;
//        Assert.assertEquals(dobijeno,ocekivano,0);
//    }
//    @Test
//    public void izracunaj45Test() throws Exception {
//        Prosek p = new Prosek();
//        List<Double> brojevi = new ArrayList<>();
//        brojevi.add(4.0);
//        brojevi.add(5.0);
//        double dobijeno = p.izracunaj(brojevi);
//        double ocekivano = 4.5;
//        Assert.assertEquals(dobijeno,ocekivano,0);
//    }
//}

package org.example;

public class Digitron {
    int a;
    int b;
    public Digitron(int a, int b){
        this.a = a;
        this.b = b;
    }
    public int saberi(){
        return a + b;
    }
    public int oduzmi(){
        return a - b;
    }
    public int pomnozi(){
        return a * b;
    }
    public int podeli(){
        return a / b;
    }
}

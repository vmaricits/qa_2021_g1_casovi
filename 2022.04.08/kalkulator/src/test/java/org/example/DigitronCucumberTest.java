package org.example;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class DigitronCucumberTest {
    Digitron d;
    int dobijeno;
    @When("sabiranje")
    public void sabiranje(){
        dobijeno = d.saberi();
    }
    @Given("kreiramo kalkulator")
    public void kreiranjeObjekta(){
        d = new Digitron(8,2);
    }
    @Then("rezultat")
    public void rezultat(){
        Assert.assertEquals(dobijeno,10);
    }

    @Given("kreiramo kalkulator {int} {int}")
    public void kreiranjeSaParametrima(int a, int b){
        d = new Digitron(a,b);
    }
    @Then("rezultat {int}")
    public void rezultatSaParametrima(int ocekivano){
        Assert.assertEquals(ocekivano,dobijeno);
    }

    @When("mnozenje")
    public void mnozi(){
        dobijeno = d.pomnozi();
    }


}

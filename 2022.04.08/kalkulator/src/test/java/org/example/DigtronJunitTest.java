package org.example;

import org.junit.Assert;
import org.junit.Test;

public class DigtronJunitTest {
    @Test
    public void testSaberi(){
       Digitron d = new Digitron(8,2);
       int ocekivano = 10;
       int dobijeno  = d.saberi();
       Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testOduzmi(){
        Digitron d = new Digitron(12,2);
        int ocekivano = 10;
        int dobijeno = d.oduzmi();
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testMnozi(){
        Digitron d = new Digitron(2,5);
        int ocekivano = 10;
        int dobijeno = d.pomnozi();
        Assert.assertEquals(ocekivano,dobijeno);
    }
    @Test
    public void testDeli(){
        Digitron d = new Digitron(20,2);
        int ocekivano = 10;
        int dobijeno = d.podeli();
        Assert.assertEquals(ocekivano,dobijeno);
    }
}

Feature:
  Testiranje digitrona

  Scenario:
    Given kreiramo kalkulator
    When sabiranje
    Then rezultat

  Scenario:
    Given kreiramo kalkulator 10 4
    When sabiranje
    Then rezultat 14
    Given kreiramo kalkulator 15 32
    When sabiranje
    Then rezultat 47

  Scenario Outline:
    Given kreiramo kalkulator <prvi> <drugi>
    When mnozenje
    Then rezultat <rezultat>

    Examples:
    | prvi | drugi | rezultat |
    | 2 | 3 | 6 |
    | 3 | 4 | 12 |

  Scenario Outline:
    Given kreiramo kalkulator <a> <b>
    When sabiranje
    Then rezultat <c>

    Examples:
    | a | b | c |
    | 2 | 3 | 5 |
    | 10 | 5 | 15 |
    | 12 | 2 | 14 |
    | 25 | 24 | 49 |
    | 1234 | 5 | 1239 |
    | 12 | 13 | 25 |

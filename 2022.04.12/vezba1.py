import http.server as server
import time
import urllib.parse as parser
import mysql.connector as conn

class Odgovor(server.SimpleHTTPRequestHandler):
    def do_POST(self):
        duzina = int(self.headers["Content-Length"])
        parametri = self.rfile.read(duzina).decode()
        parametri = parser.parse_qsl(parametri)
        parametri = dict(parametri) 
        db = conn.connect(username="root",passwd="",host="localhost",database="bank")
        cur = db.cursor()
        cur.execute(f"select * from korisnici where username = '{parametri['username']}' and password = '{parametri['password']}'")
        odgovor = 'korisnik ne postoji'
        if cur.fetchone():
            odgovor = f"dobro dosao {parametri['username']}"
        db.close()
        self.send_response(200)
        self.send_header("content-type","text/html")
        self.end_headers()
        self.wfile.write(odgovor.encode())
        
    def do_GET(self): 
        izlazni_tok = self.wfile
        izlazni_tok.write(b"HTTP/1.1 200 Ok\r\n")
        izlazni_tok.write(b"Dovla: ricma\r\n")
        izlazni_tok.write(b"Bojana: umetnica\r\n")
        izlazni_tok.write(b"Content-type: text/html\r\n")
        #izlazni_tok.write(b"Content-length: 20\r\n")
        izlazni_tok.write(b"\r\n")
        izlazni_tok.write(f"<h3>Uloguj se</h3>".encode())
        izlazni_tok.write(b"<form method='post'>")
        izlazni_tok.write(b"<input name='username' value='' type='text' />")
        izlazni_tok.write(b"<input name='password' value='' type='text' />")
        izlazni_tok.write(b"<input type='submit' value='Login' />")
        izlazni_tok.write(b"</form>")
        izlazni_tok.write(b"<script>document.write('Hello Dovla The Greatest')</script>")


server.HTTPServer(("localhost",8000),Odgovor).serve_forever()
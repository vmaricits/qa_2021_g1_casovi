from bs4 import BeautifulSoup
import requests


def test_glasanje_10_iteracija(): 
    kandidati = {} 
    for i in range(10):
        print(f"Test iteracija {i}") 
        res = requests.get(f"http://gresnik.com/rezultati.php").text 
        html = BeautifulSoup(res,features="html.parser")
        divovi = html.find_all("div")
        for div in divovi:
            spanovi = div.find_all("span")
            ime = spanovi[0].text
            glasovi = spanovi[1].text.lstrip("Broj glasova: ")
            kandidati[ime] = int(glasovi) 
        res = requests.get("http://gresnik.com/izbor.php").text
        html = BeautifulSoup(res,features="html.parser")
        akcija = html.find("form")["action"] 
        inputi = [(k["value"],k.previous.strip()) for k in html.find_all("input") if k["value"] != "Biraj"]
        for id_kandidata,ime_kandidata in inputi: 
            res = requests.post(f"http://gresnik.com/{akcija}",data={"precednik":id_kandidata}).text 
            html = BeautifulSoup(res,features="html.parser")
            link = html.find("a")["href"]
            res = requests.get(f"http://gresnik.com/{link}").text 
            html = BeautifulSoup(res,features="html.parser")
            divovi = html.find_all("div")
            for div in divovi:
                spanovi = div.find_all("span")
                ime = spanovi[0].text
                glasovi = int(spanovi[1].text.lstrip("Broj glasova: "))
                if ime == ime_kandidata:
                    print(kandidati[ime_kandidata]+1 == glasovi) 
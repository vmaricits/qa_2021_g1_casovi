from bs4 import BeautifulSoup
import requests


def test_users():
    res = requests.get("http://gresnik.com/login.php").text
    html = BeautifulSoup(res,features="html.parser")
    action = html.find("form")["action"]

    for i in range(1,11):
        res = requests.post(f"http://gresnik.com/{action}",data={"username":f"user_{i}","pass":"123"}).text
        html = BeautifulSoup(res,features="html.parser") 
        span = html.find("span")

        assert span
        assert span.text == f"user_{i}"
        # if span:
        #     ime = span.text
        #     print(ime)
        # else:
        #     print("Nesto ne stima")
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time 
b = Chrome() 
ocekivano = [
    {"naziv":"Trotinet 1","cena":3000},
    {"naziv":"Trotinet 2","cena":4000},
    {"naziv":"Trotinet 3","cena":6000}
] 
strana = b.get("http://jadnik.com/vezba1.html") 
span = b.find_element(By.ID,"ph") 
b.implicitly_wait(35)
divovi = span.find_elements(By.TAG_NAME,"div") 
assert len(divovi) > 0 
for indeks,div in enumerate(divovi):
    ocekivani_div = f"{ocekivano[indeks]['naziv']}, {ocekivano[indeks]['cena']}" 
    print(ocekivani_div,div.text)
    assert ocekivani_div == div.text
b.quit()
from xmlrpc.client import TRANSPORT_ERROR
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time

def test_login():
    b = Chrome() 
    b.get("http://jadnik.com/login.php")
    username_kontrola = b.find_element(By.NAME,"username")
    password_kontrola = b.find_element(By.XPATH,"//form/input[@name='password']")
    taster = b.find_element(By.CSS_SELECTOR,"body > form > input[type=submit]:nth-child(6)")
    username_kontrola.send_keys("bojana")
    password_kontrola.send_keys("123")
    taster.click()  
    odgovor = b.find_element(By.TAG_NAME,"body")
    assert odgovor.text == "Welcome Bojana"
    b.quit()
from xmlrpc.client import TRANSPORT_ERROR
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time

slucajevi = [
    {"ime":"Bojana","gender":1,"age":30},
    {"ime":"Nikola","gender":0,"age":20},
    {"ime":"Elza","gender":1,"age":48},
    {"ime":"Uros","gender":0,"age":33},
    {"ime":"Ana","gender":1,"age":41}
] 

for slucaj in slucajevi:
    b = Chrome()
    b.get("http://jadnik.com/kardio.html")
    ime_kontrola    = b.find_element(By.NAME,"ime")
    pol_kontrola    = b.find_elements(By.NAME,"gender")
    godine_kontrola = b.find_element(By.NAME,"age")
    taster          = b.find_element(By.XPATH,"//form/input[@type='submit']")
    ime_kontrola.send_keys(slucaj["ime"])
    pol_kontrola[slucaj["gender"]].click()
    godine_kontrola.send_keys(slucaj["age"]) 
    time.sleep(2)
    taster.click()  
    time.sleep(2) 
    b.quit()
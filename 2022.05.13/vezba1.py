import requests
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import time
import threading

dobijeni_linkovi = set({"https://www.it-akademija.com/design-multimedia-department"}) 
# browser = Chrome() 
# browser.get("https://www.it-akademija.com") 
# linkovi = browser.find_elements(By.TAG_NAME,"a") 
# for link in linkovi:
#     if link.get_attribute("href").startswith("http"):
#         dobijeni_linkovi.add(link.get_attribute("href"))
        
# browser.quit() 

strana = requests.get("https://www.it-akademija.com").text
strana = BeautifulSoup(strana,features="html.parser")
linkovi = [a["href"] for a in strana.find_all("a") if not a["href"].startswith("tel:")]

def jedan_test():
    global linkovi
    browser = Chrome()
    browser.get("https://www.it-akademija.com")
    linkovi_na_strani = browser.find_elements(By.TAG_NAME,"a")
    for link in linkovi_na_strani:
        if not link.get_attribute("href") in linkovi:
            continue
        linkovi.remove(link.get_attribute("href")) 
        print("Otvaranje linka: " , link.get_attribute("href"))
        try:
            naziv = link.get_attribute("href").replace("/","_").replace(":","_").replace(" ","_")
            link.click() 
            browser.save_screenshot(f"vezba1/{naziv}.png")
        except Exception as ex:
            print("greska ", ex)
        break
    browser.quit()


for i in range(len(linkovi)):
    threading.Thread(None,jedan_test).start()
    



# while dobijeni_linkovi: 
#     browser = Chrome()
#     browser.get("https://www.it-akademija.com")
#     linkovi = browser.find_elements(By.TAG_NAME,"a")
#     for link in linkovi: 
#         if link.get_attribute("href") in dobijeni_linkovi:
#             print(link.get_attribute("href"))
#             link.click()
#             browser.save_screenshot(f"vezba1/platforma.png")
#             dobijeni_linkovi.remove("https://www.it-akademija.com/design-multimedia-department")

# for link in linkovi:
#     if link.get_attribute("href") in dobijeni_linkovi:
#         #dobijeni_linkovi.remove(link.get_attribute("href"))
#         try:
#             link.click()
#             print(type(link.get_attribute("href")))
#             naziv = str(link.get_attribute("href"))
#             naziv = naziv.replace("/","_").replace(":","_").replace(" ","_")
#             naziv = f"vezba1/{naziv}.png";
#             print(naziv)
#             browser.save_screenshot(f"vezba1/{naziv}.png")
#         except BaseException as ex:
#             print(ex)
#         break 
 

#print(dobijeni_linkovi)



#time.sleep(5) 

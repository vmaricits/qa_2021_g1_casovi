import http.server as server

podaci = {
    "12345":{
        "ime":"Bojana",
        "balance":500
    },
    "23456":{
        "ime":"Nikola",
        "balance":300
    }
}

class Handler(server.SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        if self.path.startswith("/favicon.ico"):
            super().do_GET()
            return
        if self.path.startswith("/greet"): 
            ime = self.headers["Cookie"] 
            token = ime.strip().split("=")[1] 
            self.send_response(200)
            self.end_headers()
            balance = podaci[token]["balance"]
            ime = podaci[token]["ime"] 
            self.wfile.write(f"Hello {ime}, imas {balance} rubalja na racunu".encode()) 
        else:
            ime = self.path.lstrip("/")
            self.send_response(200)
            self.send_header("Set-Cookie",f"ime={ime}; Max-Age=100")
            self.end_headers()
            self.wfile.write(b"...") 



server.HTTPServer(("localhost",8000),Handler).serve_forever()
import http.server as server

klime = {
    "lg":100,
    "mitsubishi":230,
    "vox":300
}

class Handler(server.SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        if self.path.startswith("/favicon.ico"):
            super().do_GET()
            return 
        self.send_response(200)
        #self.send_header("Content-type","application/xml")
        self.end_headers() 
        izlaz = '<?xml version="1.0" ?><klime>' 
        for k,v in klime.items():
            izlaz += f"""
                <klima>
                <naziv>{k}</naziv>
                <cena>{v}</cena>
            </klima>
            """
        izlaz += "</klime>" 
        self.wfile.write(izlaz.encode()) 



server.HTTPServer(("localhost",8000),Handler).serve_forever()
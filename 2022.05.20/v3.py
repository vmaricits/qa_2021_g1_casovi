import http.server as server

class Hendler(server.SimpleHTTPRequestHandler):
    def do_GET(self) -> None:
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b"Hello from dovla")


server.HTTPServer(("localhost",8000),Hendler).serve_forever()
import requests

svi = requests.get("http://127.0.0.1:5000/zena")
assert svi.status_code == 200
svi = svi.json()
for z in svi:
    print(z)
    zid = z["zid"]
    ime = z["ime"]
    vrednost = z["vrednost"]
    pojedinacno = requests.get(f"http://127.0.0.1:5000/zena?id={zid}")
    assert pojedinacno.status_code == 200
    zena = pojedinacno.json()
    zena = zena[0]
    assert zid == zena["zid"]
    assert ime == zena["ime"]
    assert vrednost == zena["vrednost"]
    
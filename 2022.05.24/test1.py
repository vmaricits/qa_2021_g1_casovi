import requests
import mysql.connector as conn
import random



def test_withdraw():
    db = conn.connect(host="localhost",database="wallet",username="root",passwd="")
    cur = db.cursor()
    cur.execute("truncate table wallet")
    db.commit()
    for i in range(1,1001):
        cur.execute(f"insert into wallet values (null,'user_{i}',1000)")
    db.commit()

    for i in range(1,1001):
        amount = random.randint(0,1000)
        ocekivano = float(1000 - amount)
        res = requests.post("http://127.0.0.1:5000/withdraw",json={"id":i,"amount":amount}).json()
        dobijeno = float(res["preostali_iznos"]) 
        assert ocekivano == dobijeno 
    cur.execute("truncate table wallet")
    db.commit()


import mysql.connector as conn
import time
from flask import Flask, request,jsonify
import uuid
import redis
import jwt
 
 
r = redis.Redis()


def db():
    return conn.connect(host="localhost",database="wallet",username="root",passwd="")
# cur = db.cursor() 
# cur.execute("update wallet set balance = balance - 100 where id = 2")
# db.commit()  
# db.close()

app = Flask("Moj Wallet Servis")

kljucevi = {
    "123456":10,
    "234567":10
}
korisnici = {
    "bojana":"123",
    "uros":"234"
}
sesije = {}

def validate_kljuc(telo):
    if "kljuc" not in telo:
        return False, {"status":1,"greska":"neispravan kljuc"}
    elif not telo["kljuc"] in kljucevi:
        return False, {"status":1,"greska":"kljuc ne postoji u bazi"}
    elif kljucevi[telo["kljuc"]] < 1:
        return False, {"status":1,"greska":"istekla licenca"} 
    kljucevi[telo["kljuc"]]-=1
    return True, True

@app.route("/withdraw",methods=["POST"])
def withdraw():
    print(request.get_data())
    telo = request.json
    s , odgovor = validate_kljuc(telo)
    if not s:
        return odgovor
    id_korisnika = telo["id"]
    suma = telo["amount"]
    baza = db()
    cur = baza.cursor()
    cur.execute("update wallet set balance = balance - %s where id = %s",(suma,id_korisnika))
    cur.execute("select balance from wallet where id = %s",(id_korisnika,))
    ostatak = cur.fetchone()[0]
    baza.commit()
    baza.close()
    return {"status":0,"preostali_iznos":ostatak}

@app.route("/login",methods=["POST"])
def login():
    podaci = request.json
    username = podaci["un"]
    passwd = podaci["pass"]
    if username in korisnici and korisnici[username] == passwd:
        #token = uuid.uuid4().hex
        #sesije[token] = True
        #r.set(token,"123") 
        #r.expire(token,25)
        token = jwt.encode({"exp":int(time.time())+30},"12345","HS256")
        return {"status":0,"token":token}
    else:
        return {"status":3,"greska":"Neispravni korisnicki podaci"}

@app.route("/",methods=["POST"])
def list():
    print(request.get_data())
    telo = request.json 
    if "token" not in telo or not r.get(telo["token"]):
        return {"status":4,"greska":"neispravan token"}
    
    r.expire(telo["token"],10)

    # s , odgovor = validate_kljuc(telo)
    # if not s:
    #     return odgovor  
    baza = db()
    cur = baza.cursor()
    cur.execute("select * from wallet")
    res = []
    for wid,un,bal in cur.fetchall():
        res.append({"id":wid,"username":un,"balance":bal}) 
    baza.close()
    return jsonify(res)

app.run(debug=True)

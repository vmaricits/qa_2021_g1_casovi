package org.example;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Iterator;

/*
{
        "name": "Black-footed Cat",
        "latin_name": "Felis nigripes",
        "animal_type": "Mammal",
        "active_time": "Nocturnal",
        "length_min": "1.1",
        "length_max": "1.7",
        "weight_min": "3.3",
        "weight_max": "6.5",
        "lifespan": "5",
        "habitat": "Desert, savannah, and scrubland",
        "diet": "Mice, insects, spiders, lizards, and birds",
        "geo_range": "Southern Africa",
        "image_link": "https://upload.wikimedia.org/wikipedia/commons/d/da/Zoo_Wuppertal_Schwarzfusskatze.jpg",
        "id": 36
    },
 */

public class App
{
    public static void main( String[] args ) throws JsonProcessingException {
    }
}

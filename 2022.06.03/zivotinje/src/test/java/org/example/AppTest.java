package org.example;

import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Iterator;

/**
 * Unit test for simple App.
 */

public class AppTest 
{
    static Client klijent = ClientBuilder.newClient();


    public static boolean checkfields(JsonNode json){
        if(!json.has("name")){
            return false;
        }
        if(!json.has("diet")){
            return false;
        }
        if(!json.has("id")){
            return false;
        }
        return true;
    }

    @Given("jedna random zivotinja")
    public void jednazivotinja() throws JsonProcessingException {
        WebTarget endpoint = klijent.target("https://zoo-animal-api.herokuapp.com/animals/rand");
        Response odgovor = endpoint.request().get();
        String odgovorstring = odgovor.readEntity(String.class);
        ObjectMapper om = new ObjectMapper();
        JsonNode odgovor_json = om.readTree(odgovorstring);
        Assert.assertTrue(checkfields(odgovor_json));
    }


    @Then("prebrojavanje zivotinja {int}")
    public void prebrojavanje_zivotinja(Integer ocekivano) {
        Assert.assertEquals(dobijeni_broj_zivotinja,ocekivano.intValue());
    }

    int dobijeni_broj_zivotinja = 0;

    @When("vise zivotinja {int}")
    public void visezivotinja(Integer komada) throws JsonProcessingException {
        WebTarget endpoint = klijent.target("https://zoo-animal-api.herokuapp.com/animals/rand/"+komada);
        Response odgovor = endpoint.request().get();
        String odgovorstring = odgovor.readEntity(String.class);
        ObjectMapper om = new ObjectMapper();
        ArrayNode odgovor_json = (ArrayNode) om.readTree(odgovorstring);
        dobijeni_broj_zivotinja = odgovor_json.size();
        Iterator<JsonNode> lista = odgovor_json.iterator();
        while(lista.hasNext()){
            JsonNode clan = lista.next();
            Assert.assertTrue(checkfields(clan));
        }
    }
}

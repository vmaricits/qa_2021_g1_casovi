from selenium.webdriver import Chrome, ChromeOptions
from selenium.webdriver.common.by import By
import time,threading 

filmovi = [
    {"naziv":"The seventh seal","godina":"1957"},
    {"naziv":"Big fish","godina":"2003"},
    {"naziv":"Matrix","godina":"1999"},
    {"naziv":"Pirates of the caribbean","godina":""}
]

opcije = ChromeOptions() 
opcije.add_argument("--headless")

from selenium import webdriver

def save_screenshot(driver: webdriver.Chrome, path: str = 'screenshot.png') -> None:
    # Ref: https://stackoverflow.com/a/52572919/
    original_size = driver.get_window_size()
    required_width = driver.execute_script('return document.body.parentNode.scrollWidth')
    required_height = driver.execute_script('return document.body.parentNode.scrollHeight')
    driver.set_window_size(required_width, required_height)
    # driver.save_screenshot(path)  # has scrollbar
    driver.find_element_by_tag_name('body').screenshot(path)  # avoids scrollbar
    driver.set_window_size(original_size['width'], original_size['height'])
 
def film(naziv,godina):
    browser = Chrome(options=opcije)
    browser.get("https://www.imdb.com") 
    polje_za_pretragu = browser.find_element(By.ID,"suggestion-search")
    polje_za_pretragu.send_keys(naziv) 
    time.sleep(5) 
    li = browser.find_elements(By.TAG_NAME,"li")
    for l in li:
        if l.get_attribute("id").startswith("react-autowhatever"):
            a_tag = l.find_elements(By.TAG_NAME,"a")[0] 
            film_tag = a_tag.find_elements(By.TAG_NAME,"div") 
            if film_tag: 
                godina_tag = film_tag[4]
                if godina_tag.text: 
                    if godina_tag.text.strip() == godina:
                        a_tag.click()
                        break
    fajl = naziv.replace(" ","_")
    save_screenshot(browser,f"{fajl}.png")
    browser.quit()


for f in filmovi:
    threading.Thread(None,film,args=(f["naziv"],f["godina"])).start()

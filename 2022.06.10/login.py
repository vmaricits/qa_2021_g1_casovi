import redis 
from flask import Flask
import uuid

app = Flask("Login Service")

r = redis.Redis() 

users = {
    "dovla":"123",
    "bojana":"234",
    "tanja":"345"
}

@app.route("/login/<string:un>/<string:pwd>")
def login(un,pwd):
    if users[un] == pwd:
        token = uuid.uuid4().hex
        r.set(token,un)
        r.expire(token,10)
        odgovor = {"token":token}
    else:
        odgovor = {"error":"invalid login"}
    return odgovor, {"Content-type":"application/json"}



app.run(port=8000)
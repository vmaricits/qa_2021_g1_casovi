import requests  

kategorije = ["family","office","children","college","party"]

def provera_polja(t,polja = [{"naziv":"id","tip":int},{"naziv":"excuse","tip":str},{"naziv":"category","tip":str}]):
    ispravno = True
    for polje in polja:
        if (polje["naziv"] not in t or not t[polje["naziv"]]) or (type(t[polje["naziv"]]) != polje["tip"]) :
            ispravno = False
            break
    return ispravno

def test_api(): 
    for kategorija in kategorije:
        print(f"Testiranje kategorije : {kategorija}")
        odgovor = requests.get(f"https://excuser.herokuapp.com/v1/excuse/{kategorija}")
        telo = odgovor.json() 
        for e in telo:
            assert provera_polja(e) 
            
test_api()

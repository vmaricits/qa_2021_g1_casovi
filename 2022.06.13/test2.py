import requests

jezici = ["en","de"]
for jezik in jezici:
    adresa = f"https://uselessfacts.jsph.pl/random.json?language={jezik}" 
    odgovor = requests.get(adresa) 
    json_odgovor = odgovor.json()  
    polja = ["id","text","source","source_url","language","permalink"]
    for polje in polja:
        if not polje in json_odgovor:
            print("Ne valja") 

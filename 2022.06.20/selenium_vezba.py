from os import link
from selenium.webdriver import Chrome,ChromeOptions
from selenium.webdriver.common.by import By
import time
opcije = ChromeOptions()
opcije.add_argument("--headless") 
opcije.add_argument("--no-sandbox") 
chrome = Chrome(options=opcije) 

smerovi = ["Programming","Design & Multimedia","Administration","IT Business","3D Design & CAD","Mobile Development"]

def provera_smera(smer):
    print(f"Testiranje smera...{smer}")
    link = chrome.find_element(By.LINK_TEXT,smer)
    link.click() 
    naslovi = chrome.find_elements(By.TAG_NAME,"h1") 
    print(naslovi)
    naslov = [n.text for n in naslovi if n.text.strip() == f"{smer} Department"]
    print(naslov)
    assert len(naslov) > 0
    link = chrome.find_element(By.LINK_TEXT,smer)
    assert link.get_attribute("class") == "current"
    smer1 = smer.replace(" ","_")
    chrome.save_screenshot(f"{smer1}.png")

def test_smerovi():
    chrome.get('https://www.it-akademija.com/')
    for smer in smerovi:
        provera_smera(smer)
    chrome.quit() 


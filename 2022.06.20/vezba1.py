from flask import Flask
from flask import jsonify,request


app = Flask("Sajt")

kuce = [
    {"id":1,"naziv":"Lepa kuca","cena":100},
    {"id":5,"naziv":"Ruzna kuca","cena":200},
    {"id":6,"naziv":"Najlepsa kuca","cena":300}
]

@app.route("/",methods=["GET","POST","PUT","DELETE"])
def main(): 
    if request.method.lower() == "get":
        return jsonify(kuce), {"content-type":"application/json"}
    if request.method.lower() == "post":
        sadrzaj = request.json
        kuce.append(sadrzaj)

    return jsonify(kuce), {"content-type":"application/json"}

app.run(debug=True)
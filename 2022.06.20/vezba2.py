from flask import Flask
import requests

app = Flask("Sajt")


@app.route("/")
def main():
    izlaz = "<h2>Hello from main page</h2>"
    kuce = requests.get("http://127.0.0.1:5000/").json()
    for kuca in kuce:
        izlaz += f"<div style='border:1px solid red;padding:4px;margin:4px;'>{kuca['naziv']}</div>"
    return izlaz

app.run(debug=True,port=8000)
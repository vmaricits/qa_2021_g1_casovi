from flask import Flask
from flask import jsonify,request


app = Flask("Sajt")

kuce = [
    {"id":1,"naziv":"Lepa kuca","cena":100},
    {"id":5,"naziv":"Ruzna kuca","cena":200},
    {"id":6,"naziv":"Najlepsa kuca","cena":300}
]

@app.route("/",methods=["POST"])
def main(): 
    zahtev = request.json 
    if zahtev['komanda'] == "saberi":
        rezultat = zahtev['a'] + zahtev['b']
        return {"rezultat":rezultat}, {"content-type":"application/json"}
    if zahtev['komanda'] == "gethouses":
        return jsonify(kuce), {"content-type":"application/json"}
    if zahtev['komanda'] == "addhouse":
        sadrzaj = request.json
        del sadrzaj['komanda']
        kuce.append(sadrzaj)

    return jsonify(kuce), {"content-type":"application/json"}

app.run(debug=True)
from cgi import test
import requests
from bs4 import BeautifulSoup

def test_login():
    podaci = open("testdata.txt","r").readlines()
    podaci = [tuple(p.strip().split(",")) for p in podaci] 
    for iun,ipwd,iexp in podaci:
        forma = requests.get("http://gresnik.com").text
        html = BeautifulSoup(forma,features='html.parser')
        token = html.findAll("input")[2]["value"] 
        res = requests.post("http://gresnik.com/login.php",data={"token":token,"username":iun,"password":ipwd}).text
        assert res == iexp

#test_login()

 
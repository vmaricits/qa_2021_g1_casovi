import requests

kategorije = ["office","family","children","college","party"]

def kljucevi(excuse):
    k = ['id','excuse','category']
    for kk in k:
        assert kk in excuse

def duzina(excuses,explen):
    assert len(excuses) == explen

ocekivana_duzina = 10

def test_excuses():
    for k in kategorije:
        res = requests.get(f"https://excuser.herokuapp.com/v1/excuse/{k}/{ocekivana_duzina}").json()
        duzina(res,ocekivana_duzina)
        for excuse in res:
            kljucevi(excuse)
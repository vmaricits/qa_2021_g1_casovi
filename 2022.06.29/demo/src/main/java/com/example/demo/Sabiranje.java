package com.example.demo;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
public class Sabiranje {

    @RequestMapping(value = "proba",method = {RequestMethod.POST})
    public Object proba(){
        return "cao";
    }

    /**
     * @api {get} /hello pozdravna poruka
     * @apiName Pozdrav
     * @apiGroup General
     *
     * @apiSuccess {String} Hello World
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     Hello World
     *
     */
    @RequestMapping("hello")
    public Object hello(){
        return new HashMap<String,String>(){{
            put("Hello","World");
        }};
    }

    /**
     * @api {get} /saberi/:a/:b Sabira dva broja
     * @apiName SaberiBrojeve
     * @apiGroup Kalkulator
     *
     * @apiParam {Number} a Prvi sabirak.
     * @apiParam {Number} b Drugi sabirak sabirak.
     *
     * @apiSuccess {Number} Rezultat sabiranja dva broja.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "firstname": "John",
     *       "lastname": "Doe"
     *     }
     *
     * @apiError UserNotFound The id of the User was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "error": "UserNotFound"
     *     }
     */
    @RequestMapping("saberi/{a}/{b}")
    public Object saberi(@PathVariable("a") Integer a, @PathVariable("b") Integer b){
        return a + b;
    }

    /**
     * @api {get} /pomnozi Mnozenje dva broja
     * @apiName Pomnozi brojeve
     * @apiGroup Kalkulator
     *
     * @apiQuery {Number} a Prvi mnozilac.
     * @apiQuery {Number} b Drugi mnozilac.
     *
     * @apiSuccess {Number} Rezultat mnozenja dva broja.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "rezultat": 6
     *     }
     *
     * @apiError UserNotFound The id of the User was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "error": "UserNotFound"
     *     }
     */
    @RequestMapping("pomnozi")
    public Object pomnozi(@RequestParam("a") Integer a, @RequestParam("b") Integer b){
        return "{\"rezultat\":" + ( a * b ) + "}";
    }
}

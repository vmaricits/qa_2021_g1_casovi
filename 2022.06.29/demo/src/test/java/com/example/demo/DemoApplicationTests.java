package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	Sabiranje sabiranje;

	@Test
	void testSabiranja(){
		int ocekivano = 5;
		int dobijeno = Integer.parseInt(sabiranje.saberi(2,3).toString());
		Assert.isTrue(ocekivano==dobijeno,"Ocekivano jednako dobijeno");
	}

	@Test
	void contextLoads() {
	}

}

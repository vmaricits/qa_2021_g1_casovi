import requests

def test_kljuceva():
    kljucevi = ["idPredmeta","naziv","slika","materijal","cena","opis","idKategorija"]
    idovi = []
    res = requests.get("http://javascript.rs/api.php").json() 
    for predmet in res:
        for kljuc in kljucevi:
            assert kljuc in predmet   

def test_kategorije():
    kategorije = {} 
    res = requests.get("http://javascript.rs/api.php").json() 
    for clanak in res:
        if clanak["idKategorija"] in kategorije:
            kategorije[clanak['idKategorija']]+=1
        else:
            kategorije[clanak['idKategorija']]=1 
    for k,v in kategorije.items():
        clanci = requests.get(f"http://javascript.rs/api.php?cat={k}").json()
        assert len(clanci) == v
from lib import voda

def test_voda_hladno():
    ocekivani_odgovor = "Nema kupanja"
    dobijeni_odgovor = voda(10)
    assert ocekivani_odgovor == dobijeni_odgovor

def test_voda_toplo():
    ocekivani_odgovor = "Ima kupanja"
    dobijeni_odgovor = voda(30)
    assert ocekivani_odgovor == dobijeni_odgovor